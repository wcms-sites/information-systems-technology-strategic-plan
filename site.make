core = 7.x
api = 2

; uw_cfg_ist_strategic_plan
projects[uw_cfg_ist_strategic_plan][type] = "module"
projects[uw_cfg_ist_strategic_plan][download][type] = "git"
projects[uw_cfg_ist_strategic_plan][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_ist_strategic_plan.git"
projects[uw_cfg_ist_strategic_plan][download][tag] = "7.x-1.0"
projects[uw_cfg_ist_strategic_plan][subdir] = ""

; cacheexclude projects[cacheexclude][type] = "module"
projects[cacheexclude][download][type] = "git"
projects[cacheexclude][download][url] = "https://git.uwaterloo.ca/drupal-org/cacheexclude.git"
projects[cacheexclude][download][tag] = "7.x-2.4"
projects[cacheexclude][subdir] = ""
